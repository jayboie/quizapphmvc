-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 26, 2014 at 10:02 AM
-- Server version: 5.6.13
-- PHP Version: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `myapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `questionid` int(100) NOT NULL,
  `answer` text NOT NULL,
  `point` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=160 ;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`id`, `questionid`, `answer`, `point`) VALUES
(1, 1, 'Strongly Agree', 2),
(2, 1, 'Agree', 2),
(3, 1, 'Neutral', 2),
(4, 1, 'Disagree', 2),
(5, 1, 'Strongly disagree', 2),
(6, 2, 'You can''t buy what you want as soon as you see it', 2),
(8, 2, 'You can''t identify all your transactions on your bank statement', 6),
(11, 3, 'Relaxation', 2),
(12, 3, 'Balance', 4),
(13, 4, 'You wouldn''t know how well to manage all that money, it would be too complicated', 2),
(15, 5, 'iphone', 2),
(16, 5, 'bbm', 2),
(17, 5, 'sony', 2),
(18, 5, 'techno', 2),
(19, 5, 'samsung', 2),
(20, 6, 'Pull out your calculator to calculate how much everyone owes. all man for himself', 2),
(21, 6, 'You pay for those who have less than you do', 4),
(23, 7, 'gmail google', 10),
(24, 7, 'aol', 10),
(26, 8, 'yes', 6),
(27, 8, 'no', 2),
(30, 10, 'kfc', 7),
(31, 10, 'mcdonalds', 9),
(32, 10, 'biggs', 9),
(33, 10, 'lafiya', 7),
(112, 101, 'image101-9.jpg', 4),
(119, 101, 'image101-10.jpg', 4),
(122, 4, 'You might spend it al without saving much', 5),
(123, 4, 'Getting rich changes people and not for the better', 7),
(124, 4, 'You could loose all your money and end up with nothing', 9),
(126, 6, 'You pay for everyone on the table and for new friends you just met', 6),
(127, 6, 'You quickly bring out your wallet without checking how much the bill is', 8),
(128, 109, 'Financial success, i don''t think that applies to my situation', 2),
(129, 109, 'Can we talk about this later?', 4),
(130, 109, 'Careful money management', 6),
(131, 109, 'My spiritual life is more important to me right now', 8),
(132, 3, 'Investments or Savings', 6),
(133, 3, 'Automation so you dont miss any more bills', 8),
(134, 110, 'Money come, money go', 2),
(135, 110, 'Money is a devils tool', 4),
(136, 110, 'Save it for the dry season', 6),
(137, 110, 'Managing money is overwhelming', 8),
(138, 111, 'Donate most or all of it for a good cause', 2),
(139, 111, 'Put in the bank, or buy low risk investments', 4),
(140, 111, 'Take your friends to New york because you don HAMMER!!', 6),
(141, 111, 'You get confused and do nothing at the moment', 8),
(142, 112, 'image112-1.jpg', 2),
(144, 2, 'You can''t afford to treat your friends and family', 4),
(145, 113, 'image113-1.jpg', 2),
(146, 113, 'image113-2.jpg', 4),
(147, 113, 'image113-3.jpg', 6),
(148, 113, 'image113-4.jpg', 8),
(149, 112, 'image112-2.jpg', 4),
(150, 112, 'image112-3.jpg', 6),
(151, 112, 'image112-4.jpg', 8),
(152, 112, 'image112-5.jpg', 1),
(153, 114, 'I am doing o.k. with what I make, but a little more to put aside', 2),
(154, 114, 'I could use a lot more than I am making right now to get the things I have always wanted. ', 4),
(155, 114, 'Everyone can always use more money, I am just not sure how much more I would need', 6),
(156, 115, 'I always seem to end up short.', 2),
(157, 115, 'I can tell you how much I will have left almost to the penny. ', 4),
(158, 115, 'I am never sure if I will come out a bit ahead or a bit behind each month.', 6),
(159, 115, 'I don’t keep track of my money, but I know that I will come out ahead each month. ', 8);

-- --------------------------------------------------------

--
-- Table structure for table `paper`
--

CREATE TABLE IF NOT EXISTS `paper` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `QUESTION` text NOT NULL,
  `TYPE` text NOT NULL,
  `hashquestion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=116 ;

--
-- Dumping data for table `paper`
--

INSERT INTO `paper` (`id`, `QUESTION`, `TYPE`, `hashquestion`) VALUES
(2, 'You get uncomfortable when?', 'text', ''),
(3, 'One thing you feel your financial status need right now is', 'text', ''),
(4, 'If you had to pick your #1 concern about getting wealthy, it would be', 'text', ''),
(6, 'You and your friends went to a joint and the bill comes, you:', 'text', ''),
(109, 'What plays the important role in your financial success', 'Text', '8063d31bd1c2b7f26798e329bcde0efc'),
(110, 'Your belief about money is', 'Text', '5515d9c0fda978a4c888c69719202d27'),
(111, 'You suddenly come into a large amount of money what do you do?', 'Text', 'e013f426a5e4825f0d9aff046b08dad5'),
(112, 'Which email serves you best', 'Image', '78217e910fb00acfa990065f26f97437'),
(113, 'where do you likely get your fastfood', 'Image', 'b69c255b5e4a7b8368064f67035628d2'),
(114, 'which of this describes how your income is', 'Text', '29be7a92eaf24525ac42192547848070'),
(115, 'At the end of each month', 'Text', '415bd3d710d7637c21426d55d0386506');

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE IF NOT EXISTS `result` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `result` text NOT NULL,
  `point` int(100) NOT NULL,
  `resulthead` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`id`, `result`, `point`, `resulthead`) VALUES
(1, 'Starting up and heading innovative and inventive projects comes naturally to you. You like to spend on anything and everything that inspires you; having interesting people over to dinner, travel, books, laughter, international speakers and your ideas for personal projects. Your house is not necessarily neat and ordered, and can be packed with the latest gadgets, books and fads. You love variety, high-energy environments and situations filled with fun, action and excitement.         ', 45, 'Entrepreneur '),
(2, 'You are a natural financial leader. That is, you are good at spotting trends and like to be in control of your financial investments and projects as much as possible. In fact, compared to many others, you find it easy to investigate money matters such as your future investment options. You are usually at the centre of financial matters and you are therefore often sought out by people for financial help. You respond to others by being generous, free and open with useful financial advice.         ', 55, 'Money mentor'),
(3, 'Male or female you will most likely be the financial controller of your family finances. You are not afraid to discuss money matters. You are often in the know when it comes to investments and sound money making ventures. Your ability to see the big financial picture means you can set long-term goals.You prefer financial advice that is cutting edge and delivered by competent experts. You are usually well informed and interested in learning about money matters. You are not afraid of complexity and you are probably part of the minority when it comes to being able to digest the financial pages of the newspaper.         ', 70, 'Financial controller '),
(4, 'You are a ''money mechanic'' because you are great at adjusting your finances to do the best you can with what you''ve got. You like to spend your money on the outdoors where you like to get involved in independent practical activities and daring adventures. You''re not swayed by hype or advertising and instead, you like to see tangible, practical products from the use of your cash. In fact, you may spend a lot on your interests and hobbies and little on anything else.You hate unnecessary effort, so you will always pick ease of methods for money payments over more complicated, labour intensive ones.         ', 50, 'Money mechanic'),
(5, 'You have lots of ''shoulds'' and ''I should save for retirement'' is just one of them. Financially, you have clear expectations, want investments that will perform on explicit, established criteria, and like to be able to make decisions and exercise control. You do better when you have a clear pathway and goal in mind and have a definite plan that leads to how to get there. You like to see progress charts and tangible records. You are particularly drawn to investments where you can see practical benefits for people eg. trust fund for your child''s education.  ', 60, 'Money Organiser'),
(6, 'You respond to the beautiful, simple and authentic things in life. Your home is your castle and you can be devoted to your family welfare. You run good family rituals such as family or work parties. You like to use your money for defining, developing who you are and who you want to become. You love to learn, grow, excel and please others. You like to spend on your many authentic interests in developing self or others. You love nature, so often want to spend on weekends away. You value your autonomy and often think that if you want something done properly you need to do it yourself.', 65, 'Authentic dreamer');

-- --------------------------------------------------------

--
-- Table structure for table `useranswer`
--

CREATE TABLE IF NOT EXISTS `useranswer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `questionid` int(11) NOT NULL,
  `answerid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=501 ;

--
-- Dumping data for table `useranswer`
--

INSERT INTO `useranswer` (`id`, `username`, `questionid`, `answerid`) VALUES
(274, 'jayboie', 2, 144),
(275, 'jayboie', 3, 132),
(276, 'jayboie', 4, 124),
(277, 'jayboie', 6, 126),
(278, 'jayboie', 109, 130),
(279, 'jayboie', 110, 135),
(280, 'jayboie', 111, 141),
(281, 'jayboie', 112, 149),
(282, 'jayboie', 113, 148),
(283, 'jayboie', 114, 155),
(284, 'jayboie', 115, 158),
(285, 'joshua', 2, 144),
(286, 'joshua', 3, 12),
(287, 'joshua', 4, 123),
(288, 'joshua', 6, 127),
(289, 'joshua', 109, 128),
(290, 'joshua', 110, 136),
(291, 'joshua', 111, 139),
(292, 'joshua', 112, 150),
(293, 'joshua', 113, 147),
(294, 'joshua', 114, 155),
(295, 'joshua', 115, 158),
(376, 'tinkerbellmyn@yahoo.com', 2, 6),
(377, 'tinkerbellmyn@yahoo.com', 3, 147),
(378, 'tinkerbellmyn@yahoo.com', 4, 153),
(379, 'tinkerbellmyn@yahoo.com', 6, 159);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(2, 'jayboie', '8ee30677a3cfe34ae8e24d4b9ac405dc'),
(3, 'jayboie', '8ee30677a3cfe34ae8e24d4b9ac405dc'),
(4, 'jayboie', '8ee30677a3cfe34ae8e24d4b9ac405dc'),
(5, 'jayboie', '21b919ca84f2ab678b68c0470c06df52'),
(6, 'jayboie', '21b919ca84f2ab678b68c0470c06df52'),
(7, 'josh', '5f4dcc3b5aa765d61d8327deb882cf99');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
