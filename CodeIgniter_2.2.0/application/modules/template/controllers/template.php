<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class template extends MX_Controller {
    function one_col($data){
        $this->load->view('one-col',$data);
    }
    
    function two_col($data){
        $this->load->view('two-col',$data);
    }
    
    function admin(){
        $this->load->view('admin');
    }
    
    function result($data){
        $this->load->view('form2',$data);
    }
}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */