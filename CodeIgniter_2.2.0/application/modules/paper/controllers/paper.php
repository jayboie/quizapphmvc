<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class paper extends MX_Controller {

 function index()
	{
	
    $this->load->model('mdl_paper');
  $query=$this->mdl_paper->get('QUESTION');
  //print_r($query);
  $data['query']=$query;
  //print_r($data['query']);
  //$data['module']="paper";
 // $data['view_file']="display";
  //$this->load->view('display',$data);
  echo modules::run('template/admin');
    }

    function result(){
      echo modules::run('result');
    }

    function users(){
      echo modules::run('useranswer');
    }

    function home(){
      $this->load->model('mdl_paper');
  $query=$this->mdl_paper->get('QUESTION');
  //print_r($query);
  $data['query']=$query;
  //print_r($data['query']);
  $data['module']="paper";
 // $data['view_file']="display";
  //$this->load->view('display',$data);
  echo modules::run('template/two_col',$data);
    }


    function questionType(){
        if($this->input->post('TYPE',TRUE)=="Text"){
            $this->submitQuestion();
        }else{
             //$this->load->module('upload');
    // echo   $this->upload->do_upload();
    $this->submitQuestion();
        }
        redirect('index.php/paper/home');
    }


    function updateQuestion(){
        
       $data = $this->input->post();
        $quid=$this->uri->segment(3);
        $questiondb=$this->get_where_custom('id',$quid);
        foreach($questiondb->result() as $row){
        $dbdata['TYPE']=$row->TYPE;
        $dbdata['hashquestion']=$row->hashquestion;
           }
        $dbdata['QUESTION']=$data['QUESTION']; 
        $dbdata['id']=$quid; 
          $this->_update($quid,$dbdata);
          echo modules::run('answer/updateAnswers',$quid);
        redirect('index.php/paper/home');
    }
    function getQuestions($id){
      $dbdata=$this->get_where($id);
      print($id).' <br/>';
      foreach ($dbdata->result() as $key) {
        return $key->QUESTION;
      }
    }
    function getType($id){
      $dbdata=$this->get_where($id);
      foreach ($dbdata->result() as $key) {
        return $key->TYPE;
      }
    }

    function submitQuestion(){
        $data = $this->input->post();
        $hash = rand(10,100);
        $hashquestion= md5($data['QUESTION'].$hash);
        //$dbdata[]=array();
        $dbdata['QUESTION']=$data['QUESTION'];
        $dbdata['TYPE']=$data['TYPE'];
        $dbdata['hashquestion']=$hashquestion;
        $dbdata['id']='';
        $this->_insert($dbdata);$quid="";
       // $answers[]=array();
        $ansdb=$this->get_where_custom('hashquestion',$hashquestion);
        foreach($ansdb->result() as $row){
            $quid= $row->id;
           }
          
           if($this->input->post('TYPE',TRUE)=="Image"){
            $bool=false;
            $ind=0;
                echo   modules::run('upload/do_upload',$quid,$bool,$ind);
                exit;
               // redirect('index.php/paper');
            }
           
           modules::run('answer/saveAnswer',$quid);
            redirect('index.php/paper/home');
        
    }
    
    function mydelete(){
       $delete_id= $this->uri->segment(3);
       if(! isset($delete_id)){
           $delete_id= $this->input->post('delete_id',$id);
               
        } 
        
        if (is_numeric($delete_id)){
           $this->_delete($delete_id);
           echo modules::run('answer/deleteAnswer',$delete_id);
           ?>
            
           <?php
         
        }
        
         redirect('index.php/paper/home');
    }
    
    
    function create(){
        $update_id= $this->uri->segment(3);
        if(! isset($update_id)){
           $update_id= $this->input->post('update_id',$id);
               
        }
 
        if (is_numeric($update_id)){
            $query = $this->get_data_from_db($update_id);
            $data['update_id']=$update_id;
            $data['query']=$query;
           //print_r($query);
            //$data = $this->get_data_from_db($update_id);
            $data['module']="paper";
            $this->load->view('data',$data);
            //exit();
        }
        else{
            $query = "";
            $data['update_id']=$update_id;
            $data['query']=$query;
           //print_r($query);
            //$data = $this->get_data_from_db($update_id);
            $data['module']="paper";
            $this->load->view('data',$data);
        }
        
       //$data = $this->get_data_from_post();
      /* $data['module']="paper";
       $data['view_file']= "form";
       echo modules::run('template/two_col',$data);*/
       
      // $this->load->view('data');
      
      
       
    }
    
    function updatestudent(){
         $data['matricno'] = $this->input->post('matricno',TRUE);
         
          
        $data['module']="paper";
       $data['view_file']= "form2";
       echo modules::run('template/two_col',$data); 
        
       
    }
    function submitupdate(){
         $this->_delete("1");
         redirect('index.php/paper');
        
    }
    
    function get_data_from_post(){
        $data['QUESTION'] = $this->input->post('QUESTION',TRUE);
        $data['TYPE'] = $this->input->post('TYPE',TRUE);
       
        return $data;
    }
    
    function get_data_from_db($update_id){
        $query = $this->get_where($update_id);
        
        foreach($query->result() as $row){
            $data['QUESTION']=$row->QUESTION;
            $data['TYPE']=$row->TYPE;
            $answers= Modules::run('answer/getAnswer',$update_id); 
            $points= Modules::run('answer/getPoint',$update_id);
            $answerid=modules::run('answer/getid',$update_id);
            $data['answers']=$answers;
            $data['points']=$points;
            $data['id']=$answerid;
           
        }
        return $data;
    }
    
    function submit(){
		$this->load->library('form_validation');
        $this->form_validation->set_rules('QUESTION', 'QUESTION', 'required|min_length[3]|xss_clean');
        //$this->form_validation->set_rules('priority', 'priority', 'required|numeric|xss_clean|max_length[2]');
	
     $update_id= $this->input->post('update_id',TRUE);
    
    	if ($this->form_validation->run() == FALSE)
		{
			$this->create();
            
		}
		else
		{
		      echo "weldone";
               $data=$this->get_data_from_post();
               
              if(is_numeric($update_id)){
                $this->_update($update_id,$data);
                
              }else{
                $this->_insert($data);
              }
              
             
              
              redirect('index.php/paper/home');
		}
	}
    
    function get_table() {
$table = "paper";
return $table;
}

function get($order_by) {
$table = $this->get_table();
$this->db->order_by($order_by);
$query=$this->db->get($table);
return $query;
}

function get_with_limit($limit, $offset, $order_by) {
$table = $this->get_table();
$this->db->limit($limit, $offset);
$this->db->order_by($order_by);
$query=$this->db->get($table);
return $query;
}

function get_where($id) {
$table = $this->get_table();
$this->db->where('id', $id);
$query=$this->db->get($table);
return $query;
}

function get_where_custom($col, $value) {
$table = $this->get_table();
$this->db->where($col, $value);
$query=$this->db->get($table);
return $query;
}

function _insert($data) {
$table = $this->get_table();
$this->db->insert($table, $data);
}

function _update($id, $data) {
$table = $this->get_table();
$this->db->where('id', $id);
$this->db->update($table, $data);
}

function _delete($id) {
$table = $this->get_table();
$this->db->where('id', $id);
$this->db->delete($table);
}

function count_where($column, $value) {
$table = $this->get_table();
$this->db->where($column, $value);
$query=$this->db->get($table);
$num_rows = $query->num_rows();
return $num_rows;
}

function count_all() {
$table = $this->get_table();
$query=$this->db->get($table);
$num_rows = $query->num_rows();
return $num_rows;
}

function get_max() {
$table = $this->get_table();
$this->db->select_max('id');
$query = $this->db->get($table);
$row=$query->row();
$id=$row->id;
return $id;
}

function _custom_query($mysql_query) {
$query = $this->db->query($mysql_query);
return $query;
}


    
    
    
}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

?>
