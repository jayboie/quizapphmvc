

<!DOCTYPE HTML>
<html>
<head>
 <title>crud operations with bootstrap and codeigniter</title>
<link rel='stylesheet' type='text/css' href="css/bootstrap.css"/>
<link rel='stylesheet' type='text/css' href="../../../assets/css/bootstrap-theme.css"/>
<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.css"); ?>">
<link rel="stylesheet" href="<?php echo base_url("assets/css/style.css"); ?>">

</head>
<body>
<div class="cont">
<div class="head">
  <div class="logg"></div>
  </div>
<br/>

<?php
$questno = count($answer);
?>
<div class="panel panel-primary" style="margin: 0px 10%; border-radius:10px; border: 3px solid #002040;">
  <div  class="panel-heading">Answers for user <?php echo $reguser; ?></div>
  <div class="panel-content" style="padding: 5px;" >
<div id="rootwizard">
                    <div class="navbar">
                      <div class="navbar-inner">
                        <div class="container">
                    <ul class="nav nav-pills">
                        <?php
                            for($index=0; $index < $questno; $index++){
                                echo' <li class=""><a href="#tab'.($index+1).'" data-toggle="tab">'.($index+1).'</a></li>';
                            }
                        ?>

                       
                        
                    </ul>
                     </div>
                      </div>
                    </div>
                    
                    <div class="tab-content">
                    <?php 
                             for($index=0; $index < $questno; $index++){
                                echo ' <div class="tab-pane resulthead" id="tab'.($index+1).'"><span style="float:left;" >Question:</span>'.$question[$index].'?';
                                echo '<br/><span style="float:left;" >Answer</span>';
                                $type[$index]= strtoupper($type[$index]);
                                if ($type[$index]=="TEXT"){
                                    echo $answer[$index];
                                }else{
                                    echo '<a href="#" class="thumbnail">
             <img style="height:180px;" src="http://localhost/quizapp/CodeIgniter_2.2.0/Uploads/'.$answer[$index].'" class="img-responsive">
        </a>';
                                }
                                echo'</div>';


                             }
                    ?>
                        
                        <ul class="pager wizard">
                            <li class="previous first" style="display:none;"><a href="javascript:;">First</a></li>
                            <li class="previous"><a href="javascript:;">Previous</a></li>
                            <li class="next last" style="display:none;"><a href="javascript:;">Last</a></li>
                            <li class="next"><a href="javascript:;">Next</a></li>
                        </ul>
                    </div>  
                </div>


</div>
</div>

</div>
</body>


<script src="<?php echo base_url("assets/js/jquery.js"); ?>"></script>
<script src="<?php echo base_url("assets/js/bootstrap.js"); ?>"></script>
<script src="<?php echo base_url("assets/js/wiz.js"); ?>"></script>
<script>
    $(document).ready(function() {
        $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('#rootwizard').find('.bar').css({width:$percent+'%'});
        }});
    }); 
    </script>
</body>
</html>