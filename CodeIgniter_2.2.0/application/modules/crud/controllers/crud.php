



<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class crud extends MX_Controller {

 function index()
	{
	echo "Hello this is tasks controller";
    $this->load->model('mdl_tasks');
    //$query=$this->mdl_tasks->get('priority');
   $data['query']=$this->mdl_tasks->get('firstname');
   $data['module']="crud";
   $data['view_file']="display";
   echo modules::run('template/two_col',$data);
   
 
    }
    
    
    function mydelete(){
       $delete_id= $this->uri->segment(3);
       if(! isset($delete_id)){
           $delete_id= $this->input->post('delete_id',$id);
               
        } 
        
        if (is_numeric($delete_id)){
           $this->_delete($delete_id);?>
           
           <?php
         
        }
        
         redirect('index.php/crud');
    }
    
    
    function create(){
        $update_id= $this->uri->segment(3);
        if(! isset($update_id)){
           $update_id= $this->input->post('update_id',$id);
               
        }
 
        if (is_numeric($update_id)){
            $data = $this->get_data_from_db($update_id);
            $data['update_id']=$update_id;
        }
        else{
            
            $data = $this->get_data_from_post();
        }
        
        
        
        
       //$data = $this->get_data_from_post();
       $data['module']="crud";
       $data['view_file']= "form";
       echo modules::run('template/two_col',$data); 
      
        
    }
    
    function updatestudent(){
         $data['matricno'] = $this->input->post('matricno',TRUE);
        $data['module']="crud";
       $data['view_file']= "form2";
       echo modules::run('template/two_col',$data);  
       
    }
    function submitupdate(){
         $this->_delete("1");
         redirect('index.php/crud');
        
    }
    
    function get_data_from_post(){
        $data['matricno'] = $this->input->post('matricno',TRUE);
        $data['firstname'] = $this->input->post('firstname',TRUE);
        $data['lastname'] = $this->input->post('lastname',TRUE);
        $data['department'] = $this->input->post('department',TRUE);
        $data['email'] = $this->input->post('email',TRUE);
       
        return $data;
    }
    
    function get_data_from_db($update_id){
        $query = $this->get_where($update_id);
        
        foreach($query->result() as $row){
            $data['matricno']=$row->matricno;
            $data['firstname']=$row->firstname;
             $data['lastname']=$row->lastname;
            $data['department']=$row->department;
             $data['email']=$row->email;
           
        }
        return $data;
    }
    
    function submit(){
		$this->load->library('form_validation');
        $this->form_validation->set_rules('firstname', 'firstname', 'required|min_length[3]|xss_clean');
        //$this->form_validation->set_rules('priority', 'priority', 'required|numeric|xss_clean|max_length[2]');
	
     $update_id= $this->input->post('update_id',TRUE);
    
    	if ($this->form_validation->run() == FALSE)
		{
			$this->create();
            
		}
		else
		{
		      echo "weldone";
               $data=$this->get_data_from_post();
               
              if(is_numeric($update_id)){
                $this->_update($update_id,$data);
                
              }else{
                $this->_insert($data);
              }
              
             
              
              redirect('index.php/crud');
		}
	}
    
    function get_table() {
$table = "crud";
return $table;
}

function get($order_by) {
$table = $this->get_table();
$this->db->order_by($order_by);
$query=$this->db->get($table);
return $query;
}

function get_with_limit($limit, $offset, $order_by) {
$table = $this->get_table();
$this->db->limit($limit, $offset);
$this->db->order_by($order_by);
$query=$this->db->get($table);
return $query;
}

function get_where($id) {
$table = $this->get_table();
$this->db->where('id', $id);
$query=$this->db->get($table);
return $query;
}

function get_where_custom($col, $value) {
$table = $this->get_table();
$this->db->where($col, $value);
$query=$this->db->get($table);
return $query;
}

function _insert($data) {
$table = $this->get_table();
$this->db->insert($table, $data);
}

function _update($id, $data) {
$table = $this->get_table();
$this->db->where('id', $id);
$this->db->update($table, $data);
}

function _delete($id) {
$table = $this->get_table();
$this->db->where('id', $id);
$this->db->delete($table);
}

function count_where($column, $value) {
$table = $this->get_table();
$this->db->where($column, $value);
$query=$this->db->get($table);
$num_rows = $query->num_rows();
return $num_rows;
}

function count_all() {
$table = $this->get_table();
$query=$this->db->get($table);
$num_rows = $query->num_rows();
return $num_rows;
}

function get_max() {
$table = $this->get_table();
$this->db->select_max('id');
$query = $this->db->get($table);
$row=$query->row();
$id=$row->id;
return $id;
}

function _custom_query($mysql_query) {
$query = $this->db->query($mysql_query);
return $query;
}


    
    
    
}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

?>
