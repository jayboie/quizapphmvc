<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="lolkittens" />
<link rel='stylesheet' type='text/css' href="css/bootstrap.css"/>
<link href="css/docs.min.css" rel="stylesheet"/>

<link rel='stylesheet' type='text/css' href="style.css"/>
	<link href="css/prettify.css" rel="stylesheet"/>
	<title>Personality Test</title>
</head>
<body role="document">

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div >
        <div class="navbar-header">
          
          <a class="navbar-brand" href="#">My Social Status</a>
        </div>
        <div class="navbar-collapse collapse" id="ll">
          <ul class="nav navbar-nav">
            <li id="res"><a class="navbar-brand" ><span class="glyphicon glyphicon-repeat"></span>
          <span >Restart Test</span></a>
        </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

ddd

<div class="paper">
<header id="question" class="header"></header>
<div id="answers">
</div>

<div class="progress">
  <div class="progress-bar progress-bar-positive progress-bar-striped active" id="level" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
    <span class="sr-only">40% Complete (success)</span>
  </div>
</div>
 
<div class="col-xs-6 next"><button style="width: 70%;" class="btn btn-primary button-block pull-right">Next</button></div>
</div>
</div>

<footer style="background-color: black; height: 50px; ">
</footer>

<?php require_once('inc.php'); 
$data = new questions();
?>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/custom.js"></script>
<script>
var quiz = quizApp;
total = 0;
quiz.data = <?php echo json_encode($data->questionAndAnswers()); ?>;
quiz.getQuestionAndAnswers().showQuestion().showAnswer().reload();
selOption=''; selValue=0;
$('div.next > .btn').click(function(e){
     total += 10;
     $("#level").css("width",total+"%"); 
    e.preventDefault();
    quiz.next();
    quiz.getAnswer(selOption);
    quiz.getQuestionAndAnswers().showQuestion().showAnswer().reload().getAnswer(selOption).radimg()
   // quiz.getAnswer(sd);
   	
});
$('div.previous > .btn').click(function(e){
    
    quiz.previous();
    quiz.getQuestionAndAnswers().showQuestion().showAnswer().reload()
   
})

$(document).on('click','li',function(){
           selOption=$(this).text();
           selValue = $(this).val();
                    })

</script>

</body>
</html>