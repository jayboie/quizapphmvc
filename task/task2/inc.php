<?php

/**
 * questions
 * 
 * @package quiz App
 * @author Joshua
 * @copyright 2014
 * @version $Id$
 * @access public
 */
class questions{
    private $db;
    function __construct($host = 'localhost',$user = 'root',$pass = '',$database = 'myapp'){
        $this->db = $this->connect();
    }
    
    function connect($host = 'localhost',$user = 'root',$pass = '',$database = 'myapp'){
        // Connect to db
        $db = new mysqli($host,$user,$pass,$database);
        if  (mysqli_connect_errno()) {
            echo "Error: Could not connect to database. Please try again later.";
        exit;
        }
        return $db;
    }
    
    /**
     * questions::getQuestions()
     * get questions from database
     * @return void
     */
    function getQuestions(){
        $query = "select * from paper";
        $result = $this->db->query($query);
        $questions = array();
        while($row = $result->fetch_assoc()){
            $questions[] = $row;
        }
        return $questions;
    }
    
    
    function getAnswers($questionID){
        $query = "select * from answer where questionid = ".$questionID;
        $result = $this->db->query($query);
        $answers = array();
        while($row = $result->fetch_assoc()){
            $answers[] = $row;
        }
        return $answers;
    }
    
    function questionAndAnswers(){
        $questions = $this->getQuestions();
        $questionAndAnswer = array();
        foreach($questions as $question){
            $answers = $this->getAnswers($question['id']);
            $question['answers'] = $answers;
            $questionAndAnswer[] = $question;     
        }
        return $questionAndAnswer;
    }
    
}

?>