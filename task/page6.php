<!DOCTYPE HTML>
<html>
<head>
  <meta http-equiv="content-type" content="text/html" />
  <meta name="author" content="lolkittens" />
<link rel='stylesheet' type='text/css' href="css/bootstrap.css"/>
<link href="css/docs.min.css" rel="stylesheet"/>
  <link href="css/icon.css" rel="stylesheet"/>

<link rel='stylesheet' type='text/css' href="style.css"/>
  <link href="css/prettify.css" rel="stylesheet"/>
  <title>Personality Test</title>
</head>
<body role="document" >
  <div class="head">
<div class="logg"></div>
  </div>
  <div class="cont">
<div class="paper panel">
  
  <div class="panel-header"><header id="question" class="header"></header></div>
  <div class="panel-content" style="text-align:center;">
    <div id="answers" style=" text-align:center;"></div>
    
<div style=" padding:10px;  text-align:center;">
    
    <div class=" previous"><button style=" display:inline-block; margin-right:10px; " class="btn btn-primary button-block pull-left glyphicon glyphicon-arrow-left">&nbsp;Previous</button></div>
    
    <div class="next  "><button style="display:inline-block; margin-left:10px;" class="btn btn-success button-block pull-right ">Next &nbsp;<span class="glyphicon glyphicon-arrow-right"></span></button></div>
    
    <div class="progress" style="margin:10px 0px; ">
  <div class="progress-bar progress-bar-positive progress-bar-striped active" id="level" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
  </div>
      
</div>  
    
    </div>
    

        
</div>
</div>
</div>
<footer class="foot">Social Status quiz</footer>

<?php require_once('inc.php'); 
$data = new questions();
?>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/custom.js"></script>
<script>
var quiz = quizApp;
total = 0;
quiz.data = <?php echo json_encode($data->questionAndAnswers()); ?>;
quiz.getQuestionAndAnswers().showQuestion().showAnswer().reload();
selOption=''; selValue=0; sel=''; totpoint=0;
  myemail= "<?php echo $_GET['v1'];?>" ;
  quiz.total=quiz.data.length;
  increment=100/quiz.data.length;
  total=increment;
  $("#level").css("width",increment+"%"); 

$('div.next > .btn').click(function(e){
  //alert(quiz.data.length);
  if (! hasSelected){
    //return;
  }
     total += increment;
     $("#level").css("width",total+"%"); 
    e.preventDefault();
    
   quiz.getOption(sel);
    quiz.getAnswer(selOption);
    quiz.getUsername(myemail);
    totpoint += parseInt(selValue);
    //alert(totpoint);
    hasSelected=false;
    quiz.getTotalPoint(totpoint);
    quiz.next();
    quiz.getQuestionAndAnswers().showQuestion().showAnswer().reload()
   // quiz.getAnswer(sd);
   // alert(quiz.curt);
  // $('#' + quiz.curt).click();
    
 
  // $('#r' + quiz.curt).click();
    
});
$('div.previous > .btn').click(function(e){
  total -= increment;
     $("#level").css("width",total+"%"); 
    quiz.previous();
 // quiz.getOption(sel);
    quiz.getQuestionAndAnswers().showQuestion().showAnswer().reload()
   // alert(quiz.curt);
   totpoint-=selValue;
   
   $('#' + quiz.curt).click();
  
})

hasSelected=false;
$(document).on('click','li',function(){
           selOption=$(this).text();
           selValue = $(this).val();
            sel=$(this).attr('id');
            hasSelected=true;
         // alert(selValue);
          //  alert(sel);
   //$('div.next > .btn').click();
  // alert($(this).attr('id'));
  //$('div.next > .btn').click();
                    })
$(document).on('click','.btn-radio',function(){
            hasSelected=true
           selOption=$(this).text();
           selValue =parseInt($(this).val());
           //alert(selValue);
          sel=$(this).attr('id');
         // alert(sel);
          $(".btn-radio").css("opacity","1")
          $(this).css("opacity","0.4")
  
                    })

$("#reload").click(function(){
  location.reload();  
})
</script>

</body>
</html>